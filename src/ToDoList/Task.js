import React from 'react';
import './Task.css';

const Task = props => {
    return (
        <div className="todo">
            <div>
                <div>
                    <input type="checkbox" name="checkbox" checked={props.checkTask} onChange={props.onChangeCheckbox}/>
                    {props.task}
                </div>
            </div>
            <div>
                <button onClick={props.onDelete} className="remove-btn">Delete</button>
            </div>
        </div>
    );
};

export default Task;