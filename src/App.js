import './App.css';
import {useState} from "react";
import Task from "./ToDoList/Task";
import AddTaskForm from "./ToDoList/AddTaskForm";
import {nanoid} from "nanoid";

const App = () => {
    const [todoList, setTodoList] = useState([
        {task: "Buy milk", id: 1, checked: false},
        {task: "Walk with dog", id: 2, checked: false},
        {task: "Do homework", id: 3, checked: false},
    ]);

    const handleSubmit = (e) => {
        e.preventDefault();

        setTodoList([...todoList, {task: e.target.task.value, id: nanoid(), checked: false}]);
    }

    const soldCheckbox = (id, check) => {
        const value = check.type === 'checkbox' ? check.checked : check.value;

        setTodoList(todoList.map((task) => {
            if (task.id === id) {
                return {...task, checked: value};
            }

            return task;
        }));
    };

    const deleteTask = id => {
        setTodoList(todoList.filter(p => p.id !== id));
    }

    const todoLists = todoList.map(list => (
        <Task
            key={list.id}
            task={list.task}
            onDelete={() => deleteTask(list.id)}
            checkTask={list.checked}
            onChangeCheckbox={e => soldCheckbox(list.id, e.target)}
        />
    ));

    return (
        <div className="container">
            <div className="form-block">
                <AddTaskForm onSubmit={e => handleSubmit(e)}/>
            </div>
            <div className="list-block">
                {todoLists}
            </div>
        </div>
    )
};

export default App;
