import React from 'react';
import './AddTaskForm.css';

const AddTaskForm = props => {
    return (
        <form onSubmit={props.onSubmit} className="form">
            <input type="text" name="task" className="task-field"/>
            <button type="submit" className="btn">Add</button>
        </form>
    );
};

export default AddTaskForm;